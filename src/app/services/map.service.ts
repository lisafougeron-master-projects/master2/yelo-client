import {Injectable} from '@angular/core';

import {
    Map,
    tileLayer,
    Marker,
    marker,
    DivIcon,
    DivIconOptions,
    Util,
    divIcon,
    LatLngExpression, icon
} from 'leaflet';

import {Geolocation, GeolocationOptions, Geoposition} from '@ionic-native/geolocation/ngx';

declare var require: any;
const templateIcon = require('!raw-loader!../../assets/icon/Marker.svg');

@Injectable({
    providedIn: 'root'
})
export class MapService {
    markers: Marker[] = []; // marqueurs affichées sur la carte (stations)
    myPosition: Coordinates;
    myPositionMarker: Marker; // marqueur correspondant à la posiiton de l'utilisateur

    // description d'une station
    description = {
        free_bikes: 0,
        name: '',
        emptySlots: 0,
        freeBikes: 0
    };

    distanceWith = -1; // distance entre la station et nous (position gps requise)

    constructor(private geolocation: Geolocation) {
    }

    /**
     * Création de la map à afficher
     */
    public createMap(map: Map): Map {
        // Création de la map + centrage sur la ville de La Rochelle
        map = new Map('map').setView([46.1558, -1.1532], 12);

        // on définit ensuite la tuile à charger avec le niveau de zoom voulu
        tileLayer('http://c.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
            attribution: '<a href="//osm.org/copyright">OpenStreetMap</a>',
        }).addTo(map);

        // Permet de recalculer l'affichage de la tuile (nécessaire car délai trop court entre init map et affichage tuile)
        // issue github associé : https://github.com/Leaflet/Leaflet/issues/4835
        setTimeout(() => {
            map.invalidateSize();
        }, 100);

        this.addEvents(map);

        return map;
    }

    /**
     * Ajoute l'ensemble des événements nécessaires au fonctionnement de la carte
     */
    private addEvents(map: Map): void {
        map.on('moveend', () => {
            map.invalidateSize();
        });
    }

    /**
     * Récupération de la position courante de l'utilisateur
     */
    public getCurrentPosition(): Promise<Coordinates> {
        return new Promise((resolve, reject) => {
            const options: GeolocationOptions = {
                enableHighAccuracy: true,
                maximumAge: 10000,
                timeout: 10000
            };

            this.geolocation.getCurrentPosition(options)
                .then((position: Geoposition) => {
                    if (position !== null && position !== undefined) {
                        this.myPosition = position.coords;
                        resolve(this.myPosition);
                    }
                }).catch((error) => {
                console.error(`Erreur durant la récupération de la position de l'utilisateur : ${error}`);
                reject(error);
            });
        });
    }


    /** ================================
     *  Gestion des markers
     ** ============================= */

    /**
     * Création d'un marker d'une station
     *
     * @param station   informations d'une station
     */
    public createMarkerStation(station): Marker {
        return marker(
            [station.latitude, station.longitude], // coordonnées du point
            {
                icon: this.createIcon(station.freeBikes, station.emptySlots) // icon à afficher
            }
        );
    }

    /**
     * Création d'un marker pour la position actuelle de l'utilisateur
     *
     * @param myposition    position
     */
    public createMarkerMyPostion(myposition: Coordinates): Marker {
        return marker(
            [myposition.latitude, myposition.longitude],
            {
                icon: icon({
                    iconUrl: 'assets/marker-icon.png',
                    iconSize: [40, 60], // taille de l'icone
                    iconAnchor: [15, 40], // point de l'icone qui correspondra à la position du marker
                }),
                zIndexOffset: 1000
            }
        );
    }

    /**
     * Retire les marqueurs de toutes les stations
     */
    public removeAllMarker(map: Map): void {
        this.markers.forEach((markerToAdd: Marker) => {
            map.removeLayer(markerToAdd);
        });

        this.markers = [];
    }

    /**
     * Création de l'icon d'un marker
     *
     * @param freeBikes   nombre de vélos disponibles
     * @param emptySlots   nombre de places disponibles pour déposer un vélo
     */
    private createIcon(freeBikes: number, emptySlots: number): DivIcon {
        // On définit la couleur de l'icon en fonction du nombre de vélo dispo
        const iconColor = freeBikes === 0 ? '#A0A0A0' : '#FFD808';

        // Couleur pour les vélos dispo
        let freeBikeColor = '#7C7C7C';
        if (freeBikes >= 3) {
            freeBikeColor = '#184F18';
        } else if (freeBikes >= 1) {
            freeBikeColor = '#7F2121';
        }

        // Couleur pour les places libres
        let emptySlotColor = '#7F2121';
        if (emptySlots >= 3) {
            emptySlotColor = '#184F18';
        } else if (emptySlots >= 1) {
            emptySlotColor = '#BF6819';
        }

        const digit = freeBikes >= 10 ? 145 : 161;

        const iconSettings = {
            mapIconUrl: templateIcon,
            iconColor: iconColor,
            numberFreeBikeColor: freeBikeColor,
            nbrFreeBikes: freeBikes,
            emptySlotColor: emptySlotColor,
            postionDigit: digit
        };

        // icon normal state
        const divOptions: DivIconOptions = {
            className: 'leaflet-data-marker',
            html: Util.template(iconSettings.mapIconUrl, iconSettings),
            iconAnchor: [25, 50],
            iconSize: [50, 50],
            popupAnchor: [0, -28]
        };

        return divIcon(divOptions);
    }

    /**
     * Met à jour la description affichée sur la carte
     *
     * @param station   station sur laquelle on vient de cliquer
     */
    public setDescriptionStation(station): void {
        // on ajoute les informations de la station (nom, nb vélo dispo, ...)
        this.description = station;
    }

    /**
     * Calcul de la distance entre la station passé en paramètre et notre position actuelle
     *
     * @param station   informations de la station sélectionnée
     * @param map       instance de la carte affichée
     */
    public calculDistanceWithStation(station, map: Map): number {
        const coordMyPos: LatLngExpression = [this.myPosition.latitude, this.myPosition.longitude];
        const coordStation: LatLngExpression = [station.latitude, station.longitude];

        // on calcul la distance
        const dist = map.distance(coordMyPos, coordStation);
        return dist / 1000;
    }
}
