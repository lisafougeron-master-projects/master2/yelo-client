import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class StationsService {
    constructor(private http: HttpClient) {
    }

    getBikes(name: string, filter: any): Observable<any> {
        let params = new HttpParams();
        console.log('name: ' + name);
        console.log('filter: ' + JSON.stringify(filter));
        if (name !== undefined && name !== '') {
            params = params.set('search', name);
        }
        if (filter !== undefined && filter !== '') {
            if (filter.Temps !== undefined && filter.Temps !== ''
                && filter.Max !== undefined && filter.Max !== ''
                && filter.Position !== undefined && filter.Position !== '') {
                params = params.set('filter', JSON.stringify(filter));
            }
        }
        const option = {params: params};
        return this.http.get('https://yelo-api.herokuapp.com/api/v1/bikes', option);
    }

}

