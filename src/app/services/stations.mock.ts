export const DISTANCES = [
    {value: 0.2, text: '200 m'},
    {value: 0.5, text: '500 m'},
    {value: 1, text: '1 Km'},
    {value: 1.5, text: '1.5 Km'},
    {value: 2, text: '2 Km'},
    {value: 2.5, text: '2.5 Km'},
    {value: 3, text: '3 Km'},
    {value: 3.5, text: '3.5 Km'},
    {value: 4, text: '4 Km'},
    {value: 5, text: '5 Km'},
    {value: 6, text: '6 Km'},
    {value: 10, text: '10 Km'}
];

export const TEMPS = [
    {value: 5, text: '5 min'},
    {value: 10, text: '10 min'},
    {value: 15, text: '15 min'},
    {value: 20, text: '20 min'},
    {value: 25, text: '25 min'},
    {value: 30, text: '30 min'},
    {value: 35, text: '35 min'},
    {value: 40, text: '40 min'}
];
