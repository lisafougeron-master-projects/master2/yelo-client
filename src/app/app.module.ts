import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LaunchNavigator} from '@ionic-native/launch-navigator/ngx';
import {StationsService} from './services/stations.service';
import {LoaderService} from './loader/loader.service';
import {LoaderPage} from './loader/loader.page';

@NgModule({
    declarations: [AppComponent, LoaderPage],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, ReactiveFormsModule, BrowserAnimationsModule],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        Geolocation,
        LaunchNavigator
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
