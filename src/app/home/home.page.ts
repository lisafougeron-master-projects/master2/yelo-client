import {Component, OnInit} from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {Map, Marker} from 'leaflet';

import {DISTANCES, TEMPS} from '../services/stations.mock';
import {MapService} from '../services/map.service';
import {StationsService} from '../services/stations.service';
import {LoaderService} from '../loader/loader.service';
import {LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator/ngx';
import {Platform} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    // jeu de données
    optionDistance = DISTANCES;
    optionTemps = TEMPS;

    map: Map; // instance de la carte affichée
    // Données liées au formulaire de recherche
    stationsForm: FormGroup; // formulaire pour la recherche de stations
    filterStatus = false;
    transportMedium = false;

    stations = [];
    filteredStations = [];

    // affichage de la description d'une station
    descriptionDisplay = false;
    helperDisplay = false;

    stationSuggestion = []; // liste des stations correspondant à notre recherche

    constructor(private formBuilder: FormBuilder,
                public mapService: MapService,
                private launchNavigator: LaunchNavigator,
                private platform: Platform,
                private stationsService: StationsService,
                private loaderService: LoaderService) {
    }

    /**
     * Traitements à l'initialisation du composant (création)
     */
    ngOnInit(): void {
        this.initForm();

        this.loaderService.startInitLoader();
        this.map = this.mapService.createMap(this.map);

        this.stationsService.getBikes(undefined, undefined).subscribe(res => {
            this.filteredStations = res;
            this.stations = res;
            // une fois la map initialisée, on peut ajouter les markers
            this.addStationsMarker();
            this.loaderService.stopInitLoader();
        });

        // On récupère également la position de l'utilisateur
        this.getCurrentPosition();
    }

    /** =======================
     *  Gestion formulaire
     ** ===================== */

    /**
     * Initialisation du formulaire de recherche d'une station
     */
    private initForm(): void {
        this.stationsForm = this.formBuilder.group({
            stationName: ['', Validators.required],
            distance: [false, Validators.required],
            distance_max: ['', Validators.required],
            temps_max: ['', Validators.required],
        });

        this.stationsForm.controls.stationName.valueChanges.subscribe(value => {
            this.stationSuggestion = this.stations.filter(
                item => item.name.toUpperCase().includes(this.stationsForm.value.stationName.toUpperCase())
            );
        });
    }

    /**
     * Réinitialisation des données du formulaire
     */
    public resetForm(): void {
        this.stationsForm.setValue({
            stationName: '',
            distance: false,
            distance_max: '',
            temps_max: ''
        });
    }


    /** =======================
     *  Gestion markers
     ** ===================== */

    /**
     * Ajout d'un marqueur correspondant à la position de l'utilisateur
     */
    private getCurrentPosition(): void {
        // Nécessaire pour bien récupérer la position (sinon erreur sur certains terminaux)
        this.platform.ready().then(() => {
            this.mapService.getCurrentPosition().then((position: Coordinates) => {
                // Une fois la position récupérée, on l'indique sur la carte

                // Si une ancienne position existait, on la supprime
                if (this.mapService.myPositionMarker !== undefined) {
                    this.map.removeLayer(this.mapService.myPositionMarker);
                }

                this.mapService.myPositionMarker = this.mapService.createMarkerMyPostion(position);
                this.mapService.myPositionMarker.addTo(this.map);
            }).catch((error) => {
                console.log(`Erreur lors de la récupération de la position : ${error}`);
            });
        });
    }

    /**
     * Affiche les marqueurs des stations
     */
    private addStationsMarker(): void {
        this.filteredStations.forEach(station => {
            // on crée l'objet correspondant au marker que l'on va ajouter
            const mark: Marker = this.mapService.createMarkerStation(station);
            // on ajoute un évément de click pour afficher la description de la station
            mark.on('click', () => {
                this.mapService.setDescriptionStation(station);
                this.mapService.distanceWith = this.mapService.calculDistanceWithStation(station, this.map);
                this.descriptionDisplayON();
            });

            // puis on ajoute notre marker à la carte et dans notre tableau de marker
            mark.addTo(this.map);
            this.mapService.markers.push(mark);
        });
    }

    /**
     * Met à jour la liste des stations affichées
     */
    private refreshStationsMarker(): void {
        this.mapService.removeAllMarker(this.map);
        this.addStationsMarker();
    }


    /** =======================
     *  Traitements annexes
     ** ===================== */

    /**
     * Recherche les stations correspondant à la saisie effectuée
     */
    public search() {
        this.switchFilterStatus();
        this.loaderService.startMapLoader();
        const name = this.stationsForm.value.stationName;
        const max = this.stationsForm.value.distance ? this.stationsForm.value.distance_max : this.stationsForm.value.temps_max;
        const param = {
            Temps: !this.stationsForm.value.distance,
            Max: max,
            Position: {
                latitude: this.mapService.myPosition.latitude,
                longitude: this.mapService.myPosition.longitude
            },
            Type: !this.transportMedium ? 'cycling' : 'walking'
        };

        this.stationsService.getBikes(name, param).subscribe(res => {
            this.filteredStations = res;
            this.refreshStationsMarker();
            this.loaderService.stoptMapLoader();
        });
    }

    /**
     * Lancement de la navigation Google Maps vers la station sélectionnée
     *
     * @param station   station vers laquelle on veut aller
     */
    public launchItineraire(station) {
        const tm = this.transportMedium ? this.launchNavigator.TRANSPORT_MODE.WALKING : this.launchNavigator.TRANSPORT_MODE.BICYCLING;

        const options: LaunchNavigatorOptions = {
            app: this.launchNavigator.APP.USER_SELECT,
            transportMode: tm
        };

        this.launchNavigator.navigate([station.latitude, station.longitude], options);
    }


    /** =======================================================
     *  Gestion affichage (formulaire de saisie, aide, ...)
     ** ===================================================== */

    public switchFilterStatus() {
        this.filterStatus = !this.filterStatus;
    }

    public switchTransportMedium() {
        this.transportMedium = !this.transportMedium;
    }

    public descriptionDisplayON() {
        this.descriptionDisplay = true;
    }

    public descriptionDisplayOFF() {
        this.descriptionDisplay = false;
    }

    /**
     * Conditionne l'affichage du la vue d'aide (sur les markers notamment)
     */
    public switchHelperDisplay() {
        this.helperDisplay = !this.helperDisplay;
    }

}
