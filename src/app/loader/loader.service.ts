import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoaderService {
    private _initLoader: boolean = true;
    private _mapLoader: boolean = false;

    initLoaderStatus = new Subject();
    mapLoaderStatus = new Subject();

    get initLoader(): boolean {
        return this._initLoader;
    }

    set initLoader(value) {
        this._initLoader = value;
        this.initLoaderStatus.next(value);
    }

    get mapLoader(): boolean {
        return this._mapLoader;
    }

    set mapLoader(value) {
        this._mapLoader = value;
        this.mapLoaderStatus.next(value);
    }

    startInitLoader() {
        this.initLoader = true;
    }

    stopInitLoader() {
        this.initLoader = false;
    }

    startMapLoader() {
        this.mapLoader = true;
    }

    stoptMapLoader() {
        this.mapLoader = false;
    }
}
