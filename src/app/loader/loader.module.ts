import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {LoaderPage} from './loader.page';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: LoaderPage
            }
        ])
    ],
    declarations: [LoaderPage]
})
export class LoaderPageModule {
}
