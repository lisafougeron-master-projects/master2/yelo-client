import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoaderService} from './loader.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-loader',
    templateUrl: 'loader.page.html',
    styleUrls: ['loader.page.scss'],
})


export class LoaderPage implements OnInit, OnDestroy {
    initLoader: boolean = false;
    mapLoader: boolean = false;
    initLoaderSubscription: Subscription;
    mapLoaderSubscription: Subscription;

    constructor(private loaderService: LoaderService) {
    }

    /**
     * Traitements à l'initialisation du composant (création)
     */
    ngOnInit(): void {
        this.initLoaderSubscription = this.loaderService.initLoaderStatus.subscribe((value: boolean) => {
            this.initLoader = value;
        });
        this.mapLoaderSubscription = this.loaderService.mapLoaderStatus.subscribe((value: boolean) => {
            this.mapLoader = value;
        });
    }

    ngOnDestroy() {
        this.initLoaderSubscription.unsubscribe();
        this.mapLoaderSubscription.unsubscribe();
    }

}
